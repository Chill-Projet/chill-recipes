# chill-recipes

Contains the chill recipes for chill-bundles

## Update

There is an utility for compiling recipes in a form suitable for the symfony/flex plugin. But this alter the configuration
which allow to store the recipe on gitlab.

To void this, we have to restore the `index.json`'s `_links` keys in his previous state.

```bash
# replace "main" by your current branch
git ls-tree HEAD */*/* | ~/dev/symfony/recipes-checker/run generate:flex-endpoint chill-project/chill-recipes main main . --contrib
# will restore the file `index.json`. This should not be executing when publishing a recipe for a new version of chill
# BUT you have to update eventually the branch names in various places
# git checkout index.json
```

## How to develop a new version of the recipe

### Inside the recipe repository

We first create a new branch, here "chill38":

```bash
git switch -c chill38
```

Then, we initialize a new version in a new directory, from a previous one

```bash
cp -r chill-project/chill-bundles/3.0 chill-project/chill-bundles/3.8
```

You can, then make your changes.

You can, then, generate the recipes with this command:

```bash
 path/to/symfony/recipes-checker/run generate:flex-endpoint chill-project/chill-recipes main main . --contrib
```

This will update the file `index.json`. You will have to **manually** fix the branch names, and keep the links related to gitlab:

```diff
     "aliases": [],
     "recipes": {
         "chill-project/chill-bundles": [
-            "3.0"
+            "3.0",
+            "3.8"
         ],
         "chill-project/chill-deploy": [
             "1.0"

// ...

     },
     "recipe-conflicts": [],
     "versions": [],
-    "branch": "main",
+    "branch": "chill38",
     "is_contrib": true,
     "_links": {
         "repository": "gitlab.com/Chill-Projet/chill-recipes",
-        "origin_template": "{package}:{version}@gitlab.com/Chill-Projet/chill-recipes:main",
-        "recipe_template": "https://gitlab.com/api/v4/projects/57371968/repository/files/{package_dotted}.{version}.json/raw?ref=main"
+        "origin_template": "{package}:{version}@gitlab.com/Chill-Projet/chill-recipes:chill38",
+        "recipe_template": "https://gitlab.com/api/v4/projects/57371968/repository/files/{package_dotted}.{version}.json/raw?ref=chill38"
     }
 }
```

A new file, like `chill-project.chill-bundles.3.8.json` should also have been created: you can commit both this file, your changes, and push it to the repository:

```bash
git add chill-project/chill-bundles/3.8
git add chill-project/chill-bundles.3.8.json
git add index.json
git commit
git push -u origin chill38
```

### Inside the chill app

Inside the chill app, you have to change the address of the recipes inside composer.json:

```diff
  "extra": {
    "symfony": {
      "allow-contrib": false,
      "require": "5.4.*",
-      "endpoint": ["flex://defaults", "https://gitlab.com/api/v4/projects/57371968/repository/files/index.json/raw?ref=main"],
+      "endpoint": ["flex://defaults", "https://gitlab.com/api/v4/projects/57371968/repository/files/index.json/raw?ref=chill38"],
      "docker": true
    }
  },
```

You can, then, load the recipes.

## How to work on recipes for an unreleased version of Chill

Sometimes, it is convenient to work on an unreleased version of Chill before it was officially released. The goal: to be able to prepare it before the release.

It is possible thanks to aliases: as explained there: https://getcomposer.org/doc/articles/aliases.md

Tip: we need to change minimum-stability, in the app's `composer.json` file

```diff
{
  "type": "project",
-  "minimum-stability": "stable",
+  "minimum-stability": "dev",
+  "prefer-stable": true,
+  "repositories": [ { "type": "vcs", "url": "https://gitlab.com/Chill-projet/chill-bundles.git" } ],
  "require": {
+    // in case of branch alias
+    "chill-project/chill-bundles": "3.8.*",
+    // or inline alias
+    "chill-project/chill-bundles": "dev-xxx-branch-feature as 3.8.0",
  },
}
```
