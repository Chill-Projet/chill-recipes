#!/bin/sh

echo ""
echo "We are replacing package.json and webpack.config.js file"
echo ""

mv package.json.dist package.json
mv webpack.config.js.dist webpack.config.js

echo ""
echo "We do not use the local translator.js file"
echo ""

rm assets/translator.js

echo ""
echo "we are replacing the postgres image by the postgis one in the compose services definition"
echo ""

sed -i 's/image: postgres.*/image: postgis\/postgis:${POSTGRES_VERSION:-16}-3.4-alpine/' compose.yaml

echo ""
echo "Replacing the naming strategy of doctrine orm"
echo ""

sed -i 's/naming_strategy: doctrine.orm.naming_strategy.underscore_number_aware/naming_strategy: doctrine.orm.naming_strategy.default/' config/packages/doctrine.yaml

echo ""
echo "We are done by all the stuffes that we can fix automatically."
echo "Now, you have to read all the configuration files in config/packages and check their content."
echo ""
echo "You should start by resolving some remaining configuration changes that we cannot automate."
echo "run grep TODO -r config/ to see them"
echo ""
echo "here will be the result:"

grep TODO -r config/

echo ""
echo "Do not forget to create an admin password, see instructions in the .env file"
echo ""
echo ""
echo "Once done, you can configure migrations and start chill:"
echo ""
echo "docker compose up -d"
echo "symfony console doctrine:migrations:migrate"
echo "# ONLY IF IT IS SAFE TO REMOVE ALL DATA IN DATABASE AND REPLACE BY SOME FIXTURES:"
echo "# symfony console doctrine:fixtures:load"
echo "# start the server"
echo "symfony server:start -d"
echo ""
echo "Do not forget to read the manuals at https://symfony.com/doc/ and https://docs.chill.social"
echo ""