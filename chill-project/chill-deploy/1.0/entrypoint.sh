#!/bin/bash

#immediatly exit if a command fails:
set -e

# waiting for the database to be ready
if [ -z "${DATABASE_HOST}" ]; then
  while ! timeout 1 bash -c "cat < /dev/null > /dev/tcp/${DATABASE_HOST}/${DATABASE_PORT}"
  do
    echo "$(date) : waiting one second for database";
    sleep 1;
  done

  echo "$(date) : the database is ready";
else
  echo "we assume the database is ready";
fi


if [ $(id -u) = "0" ]; then
  { \
    echo "[www]"; \
    echo ""; \
    echo "user=${PHP_FPM_USER}"; \
    echo "group=${PHP_FPM_GROUP}"; \
  } > /usr/local/etc/php-fpm.d/zz-user.conf
fi

{ \
    echo ""; \
    echo "session.save_handler = redis" ; \
    echo "session.save_path = \"tcp://${REDIS_HOST}:${REDIS_PORT}?db=10\"" ; \
    echo "session.gc_maxlifetime = ${SESSION_LIFETIME}" ; \
} >> /usr/local/etc/php/conf.d/custom.ini

if [ "${APP_ENV}" = "prod" ]; then
  composer dump-env "${APP_ENV}"
  chmod +r /var/www/app/.env.local.php

  if [ "${PREVENT_MIGRATIONS}" != "true" ]; then
    php /var/www/app/bin/console doctrine:migrations:status
    php /var/www/app/bin/console doctrine:migrations:migrate -n
    php /var/www/app/bin/console messenger:setup-transports
    php /var/www/app/bin/console chill:db:sync-views
  fi
fi

if [ "${CLEAR_CACHE}" != "false" ]; then
  #prepare cache
  php /var/www/app/bin/console cache:clear --no-warmup
  chgrp ${PHP_FPM_GROUP} /var/www/app/var/cache -R && chmod g+rw /var/www/app/var/cache -R
  chgrp ${PHP_FPM_GROUP} /var/www/app/var/log   -R && chmod g+rw /var/www/app/var/log   -R
fi

exec "${@}"

