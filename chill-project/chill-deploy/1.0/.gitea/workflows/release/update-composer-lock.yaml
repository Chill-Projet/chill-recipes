name: Prepare release for chill app
run-name: Update composer.lock and dependencies for preparing a release

on:
    push:
        branches:
            - 'release/**'

jobs:
    update-deps:
        runs-on: ubuntu-latest
        steps:
            - name: check out repository
              uses: https://github.com/actions/checkout@v4
            - name: get the previous chill version
              # parse the composer.lock file using jq to get the chill version before the upgrade
              id: chill-before
              uses: https://github.com/sergeysova/jq-action@v2
              with:
                cmd: 'cat composer.lock | jq --raw-output ''.packages[] | select ( .name | contains ("chill-project/chill-bundles")) | .version'''
            - name: run composer update to update composer.lock
              uses: docker://gitea.champs-libres.be/chill-project/chill-skeleton-basic/base-image:latest
              with:
                  # this is where we set the command to execute
                  args: composer update --no-install
            - name: is composer.lock changed ?
              id: composer-lock-changed
              run: 'echo is_composer_lock_changed=$(git diff --name-only | grep "composer\.lock" | wc -l) >> $GITHUB_OUTPUT'
            - name: get the new chill version
              # parse the composer.lock file using jq to get the chill version after the upgrade
              id: chill-after
              uses: https://github.com/sergeysova/jq-action@v2
              with:
                cmd: 'cat composer.lock | jq --raw-output ''.packages[] | select ( .name | contains ("chill-project/chill-bundles")) | .version'''
            - name: add a changie file for the upgrade
              uses: https://github.com/miniscruff/changie-action@v2
              if: ${{ steps.composer-lock-changed.outputs.is_composer_lock_changed == 1 }}
              with:
                version: latest
                args: 'new --body "Update dependencies. Chill-bundles upgraded from ${{ steps.chill-before.outputs.value }} to ${{ steps.chill-after.outputs.value }}" --kind Release --custom "Issue=0"'
            - name: changie batch
              if: ${{ steps.composer-lock-changed.outputs.is_composer_lock_changed == 1 }}
              uses: https://github.com/miniscruff/changie-action@v2
              with:
                version: latest
                args: 'batch auto'
            - name: changie merge
              if: ${{ steps.composer-lock-changed.outputs.is_composer_lock_changed == 1 }}
              uses: https://github.com/miniscruff/changie-action@v2
              with:
                version: latest
                args: 'merge'
            - name: commit changed files
              if: ${{ steps.composer-lock-changed.outputs.is_composer_lock_changed == 1 }}
              uses: https://github.com/stefanzweifel/git-auto-commit-action@v5
              with:
                  commit_message: "update composer.lock and file (automatic update)"
                  commit_user_name: Action Bot
                  commit_user_email: bot@chill.social